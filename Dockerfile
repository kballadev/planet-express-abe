FROM python:3.9-alpine

ENV RABBITMQ_HOST planet-express-charts-rabbitmq
ENV NEO4J_HOST planet-express-charts-neo4j-community
ENV NEO4J_PASSWORD YJJFrcM4cW

ADD requirements/requirements.txt /requirements.txt

RUN \
 apk add --no-cache --virtual .build-deps gcc geos-dev musl-dev && \
 python3 -m pip install -r requirements.txt --no-cache-dir && \
 apk --purge del .build-deps


RUN pip install -r /requirements.txt
ADD src /api
COPY conf/gunicorn_starter.sh /gunicorn_starter.sh
ENTRYPOINT ["/gunicorn_starter.sh"]
