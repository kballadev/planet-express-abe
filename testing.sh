export NEO4J_PASSWORD=neo4jpassword

docker-compose -f testing/docker-compose.yaml up -d && \
sleep 5 && \
cd src && \
python -m pytest -vv -s .
docker-compose -f ../testing/docker-compose.yaml down