#!/bin/sh
gunicorn --chdir /api wsgi:app -w 2 --threads 4 --worker-class gthread --timeout 25  -b 0.0.0.0:5000