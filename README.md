[![pipeline status](https://gitlab.com/kballadev/planet-express-abe/badges/master/pipeline.svg)] [![coverage report](https://gitlab.com/kballadev/planet-express-abe/badges/master/coverage.svg)]

# planet-Express-abe

planet-express-abe is the API-REST module of Planet express project, It's works
with flask and mongodb.

## Installation

We install the python libraries from requiriments.txt with pip, we use python3
```bash
pip install -r requirements/requirements.txt
```

For development purposes you can use [koalacorp/planet-express-charts](https://github.com/KoalaCorp/planet-express-charts)


## Usage

To run the API-REST
```python
cd src && python abe.py api
```

To run the importers
```python
cd src && python abe.py importer QUEUE_NAME
```

## Contributing
Pull requests are welcome. For major changes, please open an issue first to
discuss what you would like to change.

Please make sure to update tests as appropriate.

## License
GNU GENERAL PUBLIC LICENSE Version 3, 29 June 2007
