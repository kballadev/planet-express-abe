# coding=utf-8
"""
Settings parameter for planet-express-abe"
"""
import os

# FLASK VARS
IP_HOST = os.getenv("IP_HOST", "0.0.0.0")
DOMAIN = os.getenv("DOMAIN", "localhost")
PORT_HOST = os.getenv("PORT_HOST", "5000")

RABBITMQ_HOST = os.getenv("RABBITMQ_HOST", "localhost")
RABBITMQ_PORT = os.getenv("RABBITMQ_PORT", "5672")
RABBITMQ_USER = os.getenv("RABBITMQ_USER", "user")
RABBITMQ_PASSWORD = os.getenv("RABBITMQ_PASSWORD", "eHFkTW5NdzZiSQ==")

NEO4J_HOST = os.getenv("NEO4J_HOST", "localhost")
NEO4J_PORT = os.getenv("NEO4J_PORT", "7687")
NEO4J_USER = os.getenv("NEO4J_USER", "neo4j")
NEO4J_PASSWORD = os.getenv("NEO4J_PASSWORD", "YJJFrcM4cW")


API_VERSION = "1.0"

SCORE_INCREMENT = 0.20
SCORE_MAX = 5.0
SCORE_MIN =0.1
SCORE_DEFAULT = 1.2

DEGREE_MAX = 6
DEGREE_DEFAULT = 1

TOPIC_LEN = 80
TOPIC_MAX_WORDS = 5
TOPICS_RE = r"[\+\-a-z\',\.A-Z ]+$"

TOPIC_MAX_URLS = 200

TOKEN_EXPIRED_TIME = 60*24*7
