#!/usr/bin/env python
"""
Importer module to get data from rabbitmq and insert in Neo4j
"""
from datetime import datetime
import logging
from urllib.parse import urlparse

from importers.async_consumer import Consumer
from database.models import (
    Autor,
    Domain,
    Timestamp,
    Topic,
    Url
)
from database.utils import Neo4jUtils as NU

logger = logging.getLogger(__name__)
logging.basicConfig(level=logging.INFO)


def import_data(data):
    """"
    Method to inserte data in Neo4j
    """
    url = NU.get_or_create(Url, {"name": data.get('url')})
    url.title = data.get('title')
    domain = urlparse(url.name).netloc
    domain_node = NU.get_or_create(Domain, {"domain": domain})
    
    if not url.domain.is_connected(domain_node):
        url.domain.connect(domain_node)
    url.save()

    for autor in data.get('autors'):
        autor_node = NU.get_or_create(Autor, {"name": autor.lower()})
        if not autor_node.urls.is_connected(url):
            autor_node.urls.connect(url)
        if not url.autors.is_connected(autor_node):
            url.autors.connect(autor_node)
            url.save()
        autor_node.save()

    for timestamp in data.get('date_times', []):
        timestamp = NU.get_or_create(Timestamp, {
            "timestamp": datetime.fromtimestamp(timestamp)})
        timestamp.save()
        url.timestamp.connect(timestamp)
        url.save()

    for token in data['tokenized']:
        for topic in token['entities']:
            topic_node = NU.get_or_create(Topic, {"topic": topic["word"].lower()})
            if not topic_node.url.is_connected(url):
                topic_url = topic_node.url.connect(url)
                topic_url.score = topic["score"]
                topic_url.save()
            if not url.topic.is_connected(topic_node):
                url_topic = url.topic.connect(topic_node)
                url.save()
                url_topic.score = topic["score"]
                url_topic.save()

            topic_node.save()


def run_importer(queue):
    """
    Function to run the rabbitmq consumer
    """
    consumer = Consumer(queue, import_data)
    consumer.run()
