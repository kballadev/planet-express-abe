"""
Importer test functions
"""
import json

from database.models import Topic
from importers.importer import import_data
from testing.utils import delete_nodes

class TestImportData:
    """
    Import data test class
    """
    def test_import_data(self):
        """
        test to import data in importer data function
        """
        delete_nodes()
        with open('testing/input_data/tokenized.json', 'r') as input_json:
            data = json.load(input_json)
            import_data(data)

        db_topics = [topic.topic for topic in Topic.nodes.all()]
        expected_topics = ['ceuta', 'gobierno', 'la salud prima']
        assert sorted(db_topics) == sorted(expected_topics)

        import_data(data)

        topics = Topic.nodes.all()
        assert len(topics) == 3
