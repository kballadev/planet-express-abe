"""
Neo4j utils to get urls nodes from database
"""

from neomodel import db

from database.models import config


class Neo4jUrls():
    """ 
    Wrapper to get urls from database
    """
    def __init__(self, urls):
        """
        Init method to load the filters to search urls in graph
        """
        self.urls = urls
        self.data = {
            "urls": []
        }

        self.match_query = "MATCH (u:Url) \
            WHERE id(u) IN $url_ids \
            OPTIONAL MATCH (u)-[:AUTOR]-(a:Autor) \
            RETURN id(u), u.name, u.title, collect(a.name)"

    def get_urls_nodes(self):
        """
        get urls nodes and autors to save in data
        """
        for url_node in db.cypher_query(self.match_query, params={'url_ids':self.urls})[0]:
            self.data['urls'].append(url_node)
