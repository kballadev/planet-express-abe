"""
Graph tests
"""
import json

from graph.routes import get_data_graph
from importers.importer import import_data
from testing.utils import delete_nodes


class TestOutputData:
    def check_ids(self, nodes_attrs, ids):
        for node in nodes_attrs:
            assert node['id'] in ids

    def compare_data(self, data, output_data, key):
        pairs = zip(data[key], output_data[key])

        for data_pairs, output_data_pairs in pairs:
            for key, value in data_pairs.items():
                if isinstance(value, list):
                    assert sorted(value) == sorted(output_data_pairs[key])
                else:
                    if key != "id":
                        assert value == output_data_pairs[key]

    def test_import_data(self):
        """
        testing import data and generate graph
        """
        delete_nodes()
        for tokenized in ('tokenized', 'tokenized2', 'tokenized3', 'tokenized4'):
            with open('testing/input_data/'+tokenized+'.json', 'r') as input_json:
                json_data = json.load(input_json)
                import_data(json_data)


        data = get_data_graph(['ceuta'], 1, 1.0, 5.0, None, None)

        with open('testing/output_data/ceuta.json', 'r') as output_json:
            output_data = json.load(output_json)

        self.compare_data(data, output_data, "links")
        self.compare_data(data, output_data, "nodes")

        self.check_ids(data['nodes'], data['nodes_attr'].keys())

    def test_import_score_max(self):
        """
        testing import data and generate graph
        """
        delete_nodes()
        for tokenized in ('tokenized', 'tokenized2', 'tokenized3', 'tokenized4'):
            with open('testing/input_data/'+tokenized+'.json', 'r') as input_json:
                json_data = json.load(input_json)
                import_data(json_data)


        data = get_data_graph(['ceuta'], 1, 1.0, 4.0, None, None)

        with open('testing/output_data/ceuta_score_max.json', 'r') as output_json:
            output_data = json.load(output_json)

        self.compare_data(data, output_data, "links")
        self.compare_data(data, output_data, "nodes")
        self.check_ids(data['nodes'], data['nodes_attr'].keys())


    def test_import_date_filters(self):
        """
        testing import data and generate graph
        """
        delete_nodes()
        for tokenized in ('tokenized', 'tokenized2', 'tokenized3', 'tokenized4'):
            with open('testing/input_data/'+tokenized+'.json', 'r') as input_json:
                json_data = json.load(input_json)
                import_data(json_data)


        data = get_data_graph(['ceuta'], 1, 1.0, 5.0, float(1534780622.0), float(1562823569.0))

        with open('testing/output_data/ceuta_date_filters.json', 'r') as output_json:
            output_data = json.load(output_json)

        self.compare_data(data, output_data, "links")
        self.compare_data(data, output_data, "nodes")
        self.check_ids(data['nodes'], data['nodes_attr'].keys())


    def test_import_date_filters_degrees(self):
        """
        testing import data and generate graph
        """
        delete_nodes()
        for tokenized in ('tokenized', 'tokenized2', 'tokenized3', 'tokenized4'):
            with open('testing/input_data/'+tokenized+'.json', 'r') as input_json:
                json_data = json.load(input_json)
                import_data(json_data)


        data = get_data_graph(['ceuta'], 3, 1.0, 4.0, float(1534780622.0), float(1562823569.0))

        with open('testing/output_data/ceuta_date_filters_degrees.json', 'r') as output_json:
            output_data = json.load(output_json)

        self.compare_data(data, output_data, "links")
        self.compare_data(data, output_data, "nodes")
        self.check_ids(data['nodes'], data['nodes_attr'].keys())
