"""
Urls routes to get data from neo4j
"""
from os import EX_CANTCREAT
from flask import jsonify, Blueprint

from neomodel import db

from urls.database import Neo4jUrls
from settings import TOPIC_MAX_URLS


urls = Blueprint('urls', __name__)


def get_data_urls(urls_ids):
    """
    Function to call the get_urls function to generate the graph
    """

    neo4j_util = Neo4jUrls(urls_ids)
    neo4j_util.get_urls_nodes()

    return neo4j_util.data


@urls.route('/api/urls/<urls_ids>', methods=['GET'])
def get_urls(urls_ids):
    """
    Get autors for urls
    """

    try:
        urls_ids_list = urls_ids.split(',')
    except Exception as e:
        return jsonify({"message": "Wrong urls ids format"}), 400

    if len(urls_ids_list) > TOPIC_MAX_URLS:
        return jsonify({"message": "Max urls for search is {}".format(TOPIC_MAX_URLS)}), 400

    
    urls_ids_list_ints = []
    for url_id in urls_ids_list:
        try:
            url_id_int = int(url_id)
        except Exception as e:
            return jsonify({"message": "Urls ids are not int"}), 400

        urls_ids_list_ints.append(url_id_int)

    return jsonify(get_data_urls(urls_ids_list_ints))
