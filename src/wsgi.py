"""
Wasgi module to run in gunicorn
"""
from app import create_app
from base.routes import base as base_blueprint
from graph.routes import graph as graph_blueprint
from apidoc.routes import apidoc as apidoc_blueprint
from urls.routes import urls as urls_blueprint

app = create_app()
app.register_blueprint(base_blueprint)
app.register_blueprint(graph_blueprint)
app.register_blueprint(urls_blueprint)
app.register_blueprint(apidoc_blueprint)
