from flask import jsonify, Blueprint

base = Blueprint('base', __name__)


@base.route('/health', methods=['GET'])
def health():
    """
    Health check of the application
    ---
    responses:
        200:
            description: Health Check
            schema:
                properties:
                    status:
                        type: string
                        description: OK if the api is working
    """
    json_response = {
        "status": "OK"
    }

    return jsonify(json_response)
