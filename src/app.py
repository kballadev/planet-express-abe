# coding=utf-8
"""
Initial module to run the flasks application
"""
import logging

from flask import Flask
from flask_cors import CORS


app = Flask(__name__)

logger = logging.getLogger(__name__)
logging.basicConfig(level=logging.INFO)

def create_app():
    """
    This function create the connection to the user database and return the app
    to manage the application in other modules
    """
    CORS(app)

    return app
