"""
Graph models for Neo4j with neomodel
"""

from neomodel import (
    config,
    DateTimeProperty,
    IntegerProperty,
    FloatProperty,
    StringProperty,
    StructuredNode,
    StructuredRel,
    Relationship,
    RelationshipFrom,
    RelationshipTo)

from settings import (
    NEO4J_HOST,
    NEO4J_USER,
    NEO4J_PASSWORD,
    NEO4J_PORT)


config.DATABASE_URL = 'bolt://{}:{}@{}:{}'.format(
    NEO4J_USER,
    NEO4J_PASSWORD,
    NEO4J_HOST,
    NEO4J_PORT)


CATEGORY_DEPRECATE = """
    Category was deprecated and has now been removed,
    the functionality is now achieved using the *.nodes attribute"""


class Context(StructuredNode):
    """
    Context node model
    examples: Bilogoy, Math
    """

    @classmethod
    def category(cls):
        raise NotImplementedError(CATEGORY_DEPRECATE)

    name = StringProperty(unique_index=True, required=True)

class Domain(StructuredNode):
    """
    Domain node model
    examples: arxiv.org, math.org
    """

    @classmethod
    def category(cls):
        """
        Deprecated method
        """
        raise NotImplementedError(CATEGORY_DEPRECATE)

    domain = StringProperty(unique_index=True, required=True)
    main_domain = RelationshipTo("Domain", 'HAS_MAIN_DOMAIN')

class TopicURLRel(StructuredRel):
    """
    Relation M2M between topics and urls
    """

    @classmethod
    def category(cls):
        """
        Deprecated method
        """
        raise NotImplementedError(CATEGORY_DEPRECATE)

    size = IntegerProperty(default=1)
    score = FloatProperty()


class Url(StructuredNode):
    """
    url node model
    examples: http://arvix.org/jourtnal/398472.23
    """

    @classmethod
    def category(cls):
        """
        Deprecated method
        """
        raise NotImplementedError(CATEGORY_DEPRECATE)

    name = StringProperty(unique_index=True, required=True)
    title = StringProperty(default="")
    context = RelationshipTo(Context, 'HAS_CONTEXT')
    domain = RelationshipTo(Domain, 'HAS_DOMAIN')
    topic = RelationshipFrom("Topic", 'HAS_TOPIC', model=TopicURLRel)
    autors = RelationshipFrom("Autor", 'AUTOR')
    timestamp = Relationship('Timestamp', 'TIMESTAMP')


class Timestamp(StructuredNode):
    """
    timestamp from url node model
    examples: 1589925600.0
    """

    @classmethod
    def category(cls):
        """
        Deprecated method
        """
        raise NotImplementedError(CATEGORY_DEPRECATE)

    timestamp = DateTimeProperty(unique_index=True, required=True)


class Topic(StructuredNode):
    """
    Topic node model
    examples: yau, kontsevich
    """

    @classmethod
    def category(cls):
        """
        Deprecated method
        """
        raise NotImplementedError(CATEGORY_DEPRECATE)

    topic = StringProperty(unique_index=True, required=True)
    url = RelationshipTo(Url, 'IS_IN_URL', model=TopicURLRel)


class Autor(StructuredNode):
    """
    Autors node model
    examples: C. Rosenberg
    """

    @classmethod
    def category(cls):
        """
        Deprecated method
        """
        raise NotImplementedError(CATEGORY_DEPRECATE)

    name = StringProperty(unique_index=True, required=True)
    urls = RelationshipTo(Url, 'URL')
