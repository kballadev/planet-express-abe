"""
Neo4j utils to generate dict graph from database
"""

from neomodel import db

from database.models import config
from settings import SCORE_INCREMENT


class Neo4jUtils():
    def __init__(self, topics_lower, degrees, score_min, score_max, datetime_min, datetime_max):
        """
        Init method to load the filters to search nodes in graph
        """
        self.data = {
            "nodes": [],
            "nodes_attr": {},
            "links": []
        }
        self.top_degree = degrees
        self.degrees = degrees
        self.score_min = score_min
        self.score_max = score_max
        self.datetime_min = datetime_min
        self.datetime_max = datetime_max
        self.urls_visited = []
        self.match_date_query_filter = {}

        if self.datetime_min:
            self.match_date_query_filter['datetime_min'] = self.datetime_min
            self.match_date_query_filter['datetime_max'] = self.datetime_max

            self.match_query = "MATCH (u:Url)-[:HAS_TOPIC]-(t:Topic)\
                Where id(t)=$topic_q_id\
             MATCH (u)-[:TIMESTAMP]-(tm:Timestamp)\
                WHERE tm.timestamp>=$datetime_min and tm.timestamp<=$datetime_max\
             MATCH (u)-[ht:HAS_TOPIC]-(tr:Topic)\
                WHERE id(tr)<>$topic_q_id and\
                    ht.score>=$score_min and\
                    ht.score<=$score_max and\
                    NOT id(u) IN $url_ids\
             return tr, collect(id(u)), sum(ht.score)"

            self.match_query_topics_nodes = "MATCH (u:Url)-[:HAS_TOPIC]-(t:Topic)\
                WHERE t.topic IN $topics_lower\
             MATCH (u)-[:TIMESTAMP]-(tm:Timestamp)\
                WHERE tm.timestamp>=$datetime_min\
                     and tm.timestamp<=$datetime_max\
             MATCH (u)-[ht:HAS_TOPIC]-(tr:Topic)\
                WHERE id(tr)<>id(t) and\
                    ht.score>=$score_min and\
                    ht.score<=$score_max\
             return t, collect(id(u))"
        else:
            self.match_query = "MATCH (u:Url)-[:HAS_TOPIC]-(t:Topic)\
                Where id(t)=$topic_q_id\
             Match (u)-[ht:HAS_TOPIC]-(tr:Topic)\
                where id(tr)<>$topic_q_id and\
                    ht.score>=$score_min and\
                    ht.score<=$score_max and\
                    NOT id(u) IN $url_ids\
             return tr, collect(id(u)), sum(ht.score)"

            self.match_query_topics_nodes = "MATCH (u:Url)-[:HAS_TOPIC]-(t:Topic)\
                Where t.topic IN $topics_lower\
             MATCH (u)-[ht:HAS_TOPIC]-(tr:Topic)\
                where id(tr)<>id(t) and\
                    ht.score>=$score_min and\
                    ht.score<=$score_max\
             return t, collect(id(u))"

        self.topics_lower = topics_lower

    @classmethod
    def get_or_create(cls, node, filter):
        """
        Get or create method to inserte when the node don't exists
        """
        instance_node = node.nodes.get_or_none(**filter)

        if not instance_node:
            instance_node = node(**filter)
            instance_node.save()

        return instance_node

    def get_topics_nodes(self):
        """
        Get topics nodes and url length for initial nodes
        """
        match_params = {
            'score_min': self.score_min,
            'score_max': self.score_max,
            'topics_lower': self.topics_lower
        }

        topics_nodes_lower = [ (topic, len(set(urls))) for topic, urls in db.cypher_query(self.match_query_topics_nodes,
         params={**match_params, **self.match_date_query_filter})[0]]

        return self.get_topics(topics_nodes_lower)

    def get_relations(self, topic_q):
        """
        get relations from a topic in Neo4j
        """
        topic_q_str = topic_q.get('topic')
        topics_relationed = []

        match_params = {
            'url_ids': self.urls_visited,
            'score_min': self.score_min,
            'score_max': self.score_max,
            'topic_q_id': topic_q.id}

        for topic, urls_ids, score in db.cypher_query(self.match_query,
         params={**match_params, **self.match_date_query_filter})[0]:
            self.urls_visited = list(set(self.urls_visited + urls_ids))
            self.data['links'].append({
                    "source": topic_q_str,
                    "target": topic.get("topic"),
                    "urls": urls_ids,
                    "score": score
                })
            urls_len = len(urls_ids)
            self.update_urls_len(topic_q, urls_len)
            topics_relationed.append((topic, urls_len))

        return topics_relationed

    def update_urls_len(self, topic, urls_len):
        """
        update the urls len in the data.nodes_attr
        """
        urls_len_actual = self.data["nodes_attr"][topic.id]['urls']
        self.data["nodes_attr"][topic.id]['urls'] =  urls_len_actual + urls_len


    def decrease_degree(self):
        """
        decrease degree per iteration deep
        """
        self.degrees -= 1

    def get_degree_level(self):
        """
        Get the level degree of a node
        """
        return self.top_degree - self.degrees

    def increase_score(self):
        """
        increase score per iteration deep
        """
        self.score_min = self.score_min + (self.score_max - self.score_min)*SCORE_INCREMENT

    def add_node_to_data_nodes(self, topic, urls_len):
        """
        add no to date['nodes']
        """
        if topic.id not in self.data["nodes_attr"].keys():
            self.data["nodes_attr"][topic.id] = {
                "urls": urls_len
            }
            self.data['nodes'].append({
                "contexts": [],
                "topic": topic.get("topic"),
                "id": topic.id,
                "degree": self.get_degree_level()
            })
        else:
            self.update_urls_len(topic, urls_len)

    def get_topics(self, topics):
        """
        get topics from neo4j database
        """
        topics_relationed=[]
        for topic, urls_len in topics:
            self.add_node_to_data_nodes(topic, urls_len)
            topics_relationed = topics_relationed + self.get_relations(topic)

        self.decrease_degree()
        if self.degrees:
            self.increase_score()
            self.get_topics(topics_relationed)
        else:
            for topic_relationed, urls_len in topics_relationed:
                self.add_node_to_data_nodes(topic_relationed, urls_len)
