"""
Graph routes to get data from neo4j
"""
from datetime import datetime
import re

from flask import jsonify, Blueprint

from neomodel import db

from database.utils import Neo4jUtils
from settings import (
    DEGREE_DEFAULT,
    DEGREE_MAX,
    SCORE_DEFAULT,
    SCORE_MAX,
    SCORE_MIN,
    TOPIC_LEN,
    TOPIC_MAX_WORDS,
    TOPICS_RE)


graph = Blueprint('graph', __name__)


def get_data_graph(topics_lower, int_degrees, score_min, score_max, datetime_min, datetime_max):
    """
    Function to call the get_topics function to generate the graph
    """

    neo4j_util = Neo4jUtils(topics_lower, int_degrees, score_min, score_max, datetime_min, datetime_max)
    neo4j_util.get_topics_nodes()

    return neo4j_util.data


@graph.route('/api/topics/<topics>/degrees/<degrees>/score_min/<score_min>/score_max/<score_max>/date_min/<date_min>/date_max/<date_max>',
    methods=['GET'])
@graph.route('/api/topics/<topics>/degrees/<degrees>/score_min/<score_min>/score_max/<score_max>',
    defaults={'date_min': None,
              'date_max': None},
    methods=['GET'])
@graph.route('/api/topics/<topics>/degrees/<degrees>/score_min/<score_min>',
    defaults={'score_max': SCORE_MAX,
              'date_min': None,
              'date_max': None},
    methods=['GET'])
@graph.route('/api/topics/<topics>/degrees/<degrees>/score/<score_min>',
    defaults={'score_max': SCORE_MAX,
              'date_min': None,
              'date_max': None},
    methods=['GET'])
@graph.route('/api/topics/<topics>',
    defaults={'degrees': DEGREE_DEFAULT,
              'score_min': SCORE_DEFAULT,
              'score_max': SCORE_MAX,
              'date_min': None,
              'date_max': None},
    methods=['GET'])
def get_links_topics(topics, degrees, score_min, score_max, date_min, date_max):
    """
    Get links and topics
    ---
    responses:
      200:
        description: Topics and links
        schema:
            properties:
                relations:
                    type: array
                    description: relations between topics
                    relations:
                        schema:
                            topics:
                                type: array
                                description: topics related
                            urls:
                                type: array
                                description: urls shared between the topics related
                topics:
                    type: array
                    description: topics
                    topics:
                        schema:
                            topic:
                                type: string
                                description: topic
                            context:
                                type: array
                                description: contexts of the topic

    """
    try:
        if date_min:
            datetime.fromtimestamp(int(date_min))
            datetime_min = float(date_min)
        else:
            datetime_min = None

        if date_max:
            datetime.fromtimestamp(int(date_max))
            datetime_max = float(date_max)
        else:
            datetime_max = None
    except Exception as e:
        return jsonify({"message": "Wrong dates"}), 400

    try:
        int_degrees = int(degrees)
        if int_degrees > DEGREE_MAX or int_degrees < 1:
            return jsonify({"message": "degrees values NOT PERMITTED"}), 400
    except Exception as e:
        return jsonify({"message": "degrees values NOT PERMITTED"}), 500
    
    try:    
        for score in (score_min, score_max):
            float_score = float(score)
            if float_score < SCORE_MIN or float_score > SCORE_MAX:
                return jsonify({"message": "Scores values NOT PERMITTED"}), 400
    except Exception as e:
        return jsonify({"message": "Scores values NOT PERMITTED"}), 500

    if len(topics) > TOPIC_LEN:
        return jsonify({"message": "topics length EXCEEDED"}), 400

    if not re.match(TOPICS_RE, topics):
        return jsonify({"message": "Invalid characters in topics search"}), 400

    topics_lower = [topic.lower() for topic in topics.split(',')]
    if len(topics_lower) > TOPIC_MAX_WORDS:
        return jsonify({"message": "Max words for search is {}".format(TOPIC_MAX_WORDS)}), 400

    return jsonify(get_data_graph(topics_lower, int_degrees, float(score_min), float(score_max), datetime_min, datetime_max))
