"""
Cli module to manage the application in the shell
"""

import click

from app import create_app
from base.routes import base as base_blueprint
from graph.routes import graph as graph_blueprint
from urls.routes import urls as urls_blueprint
from importers.importer import run_importer
from apidoc.routes import apidoc as apidoc_blueprint
from settings import IP_HOST, PORT_HOST


@click.group()
def cli():
    """
    Default cli command
    """

@cli.command()
def api():
    """
    Command to run the api-rest in flasks
    """
    app_instance = create_app()
    app_instance.register_blueprint(urls_blueprint)
    app_instance.register_blueprint(base_blueprint)
    app_instance.register_blueprint(graph_blueprint)
    app_instance.register_blueprint(apidoc_blueprint)
    app_instance.run(IP_HOST, PORT_HOST)


@cli.command()
@click.argument('queue')
def importer(queue):
    """
    Command to exec the importer form rabbitmq to neo4j
    """
    run_importer(queue)


if __name__ == '__main__':
    cli()
