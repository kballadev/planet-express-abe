"""
Utils module to use in testing
"""
import multiprocessing as mp
import random

from neomodel import db, clear_neo4j_database

from importers.importer import import_data


def generator_data():
    """
    Random generator data in Neo4j
    """
    with open('/usr/share/dict/words') as words_file:
        words = words_file.read().splitlines()
    pool = mp.Pool(processes=mp.cpu_count())
    words_len = len(words)
    results = []
    for url in range(1, 1000):
        entities = [
            {
                "word": words[random.randint(0, words_len-1)],
                "score": random.randint(0, 5),
                "entity": 385
            } for random_int in  range(1, random.randint(1, 8))]

        data = {
            "url": "http://{}.com".format(url),
            "autor": random.choice(words),
            "date_time": "14/11/2020",
            "title": "",
            "content": "",
            "tokenized": [
                {
                    "topics": [],
                    "names": [],
                    "entities": entities
                }
            ]
        }
        results.append(pool.apply(import_data, args=(data,)))

    print(results)


def delete_nodes():
    """
    function delete all in neo4j
    """
    clear_neo4j_database(db)
