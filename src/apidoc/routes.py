"""
Module to enable swagger in api-rest
"""

from flask import jsonify, Blueprint
from flask_swagger import swagger

from app import app

apidoc = Blueprint('apidoc', __name__)


@apidoc.route("/apidoc")
def spec():
    """
    Main swagger route
    """
    return jsonify(swagger(app))
